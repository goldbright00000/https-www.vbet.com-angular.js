<?php
$proj_dir ="beys/";        
$siteURL="http://{$_SERVER['HTTP_HOST']}/{$proj_dir}";
$adminURL="http://{$_SERVER['HTTP_HOST']}/{$proj_dir}admin/";

define("APP_NAME","Shakha");
define("PAGE_SIZE","5");
define("IMAGE_HEIGHT","800");
define("IMAGE_WIDTH","800");
define("DEFAULT_LANG","en");

define("MODE_PRODUCTION",1);
define("MODE_DEVLOPMENT",0);

define("ENABLE_SENDGRID",1);
define("SENDGRID_KEY","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");

$APP_MODE=MODE_DEVLOPMENT;

if($APP_MODE==MODE_PRODUCTION){
	error_reporting(0);
	ini_set('display_errors', 0);
}else{
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

$con;
function db_connect(){
	global $con;  
	$dbhost = "localhost";
	$dbuser = "root";
	$dbpassword = "";
	$database = "vbet";      
	$con = new PDO ( "mysql:host=$dbhost;charset=utf8;dbname=$database", "$dbuser", "$dbpassword") or die ( 'error' );
	$con->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}
$upload_path="images/uploads/";
date_default_timezone_set("Asia/Kolkata");
if(empty($_SESSION)){
	session_start();
}


$SMTP_HOST="";	////SMTP EMAIL SETTINGS
$SMTP_USER="";
$SMTP_PASSWORD="";
$SMTP_FROM="";
$SMTP_FROM_EMAIL="";
$SMTP_PORT=587;		/// Port eg. 25, 587 ...


//////////////////////Functions begin from here//////////////////////

function phpNow(){
	return date("Y-m-d H:i:s",time());
}
/*PDO FUNCTIONS*/
function getRows($sql,$params=false){
	global $con;
	$query=$con->prepare($sql);
	if($params){
		foreach($params as $k=>$v){
			$query->bindValue(":$k",$v."");
		}
	}
	$query->execute();
	if($query->rowCount()==0){
		return array();
	}else{
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
}
function getRow($sql,$params=false){
	global $con;
	$query=$con->prepare($sql);
	if($params){
		foreach($params as $k=>$v){
			$query->bindValue(":$k",$v."");
		}
	}
	$query->execute();
	if($query->rowCount()==0){
		return false;
	}else{
		return $query->fetch(PDO::FETCH_ASSOC);
	}
}

function insertRow($table,$data){
	global $con;
	$columns=array_keys($data);
	$cols=implode(",",$columns);
	$params=":".implode(",:",$columns);
	$sql="insert into $table ($cols) values($params) ";
	$query=$con->prepare($sql);
	foreach($data as $k=>$v){
		$query->bindParam(":$k",$data[$k]);
	}
	$query->execute();
	return $con->lastInsertId();
}

function updateRow($table,$data,$where=false){
	global $con;
	$sql="update $table set ";
	$first=true;
	foreach($data as $k=>$v){
		if($first){
			$sql.=" $k=:$k ";
			$first=false;
		}else{
			$sql.=", $k=:$k ";
		}
	}
	if($where){
		$sql.=" where 1 ";
		foreach($where as $w=>$wv){
			if($where[$w]==null){
				$sql.=" and $w is null ";
			}else{
				$sql.=" and $w=:$w ";
			}
		}
	}
	$query=$con->prepare($sql);
	foreach($data as $k=>$v){
		$query->bindParam(":$k",$data[$k]);
	}
	if($where){
		foreach($where as $k=>$v){
			if($where[$k]==null){
				//$query->bindValue(":$k",null);
			}else{
				$query->bindParam(":$k",$where[$k]);
			}
		}
	}
	$query->execute();
	return $query->rowCount();
}
?>
