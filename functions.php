<?php 
function uploadImage($file, $name = "") {
	global $proj_dir;
	$upload_path = getSetting ( "upload_path" );
	$image_width = getSetting ( "img_width" );
	$image_height = getSetting ( "img_height" );

	$time = md5 ( microtime () );

	// validate file
	$whitelist = array (
			".jpg",
			".jpeg",
			".gif",
			".png",
			".bmp"
	);

	$val_img = true;

	if ($val_img == false) {
		$msg = "Invalid image format. Vaild formats are: .jpg, .jpeg, .gif, .png, .bmp.";
		return "";
	} else {
		$base_name_pic = str_replace ( " ", "_", basename ( $file ['name'] ) );
		$base_name_pic = str_replace ( "#", "_", $base_name_pic );
		$file_name = $name . $time . $base_name_pic;

		$ROOT = $_SERVER ['DOCUMENT_ROOT'] . "/" . $proj_dir . $upload_path . "/";

		if (! move_uploaded_file ( $file ['tmp_name'], $ROOT . $file_name )) {
			$msg = "Error occured while uploading document.";
			echo $msg;
			return "";
		} else {
			// optimizing
			// get image size
			$path = "{$ROOT}{$file_name}";
			$arr = explode ( ".", $path );
			$extension = $arr [count ( $arr ) - 1];
				
			$imgInfo = getimagesize ( "{$path}" );
			if ($imgInfo [0] > $image_width) {
				exec ( "convert \"{$path}\" -resize " . $image_width . "x" . $image_height . " \"{$path}\"" );
			}
				
			if ($extension == "png") {
				exec ( "optipng \"{$path}\" --out \"{$path}\"" );
			} else {
				exec ( "jpegoptim \"{$path}\" --max=90 --strip-all --preserve --totals" );
			}
			return $file_name;
			// $file_path_for_pic = "/" . $file_path_for_pic;
		}
	}
}
function getsettingByKey($setting){
	global $con;
	$query=$con->prepare("SELECT `id`, `key`,  `val`
						FROM `setting`
			WHERE `key`=:setting");
	$query->bindParam(":setting", $setting);
	$query->execute();
	if($query->rowCount()>0){
		$row=$query->fetch(PDO::FETCH_ASSOC);
		return $row;
	}else{
		return false;
	}
}
function getSetting($key){
	global $con;
	$query=$con->prepare("SELECT `val`
			FROM `setting`
			WHERE `key`=:key");
	$query->bindParam(":key", $key);
	$query->execute();
	if($query->rowCount()>0){
		$row=$query->fetch(PDO::FETCH_ASSOC);
		return $row['val'];
	}else{
		return false;
	}
}
function setSettingByKey($key,$val)
{
	global $con;
	$sql = $con->prepare ( "UPDATE setting SET
							`val`=:val
 							WHERE `key`=:key" );

	$sql->bindParam ( ":val", $val);
	$sql->bindParam ( ":key", $key);
	$sql->execute ();
}
function logout(){
	$_SESSION = array();
	unset($_SESSION);
}
function getFullImage($path){
	global $siteURL;
	$upload_path=getSetting("upload_path");
	if(!empty($path)){
		return $siteURL.$upload_path.$path;
	}else{
		return ""; 	
	}
}
function getFullURL($file_path) {
	global $siteURL, $upload_path;
	
	return $siteURL . $upload_path . $file_path;
}
function getCategories($pcat_id=0,$with_count=false){
	global $con;

	$query=$con->prepare("SELECT `id`,`cat_name`,`cat_img`,`cat_desc`,`pcat_id`,`order`,`status`
		FROM `category`
		WHERE status>0 and pcat_id=:pcat_id
		ORDER BY  `order` ASC 
		");
	$query->bindParam(":pcat_id",$pcat_id);
	$query->execute();

	$rows=array();
	if($query->rowCount()>0){
		while($row=$query->fetch(PDO::FETCH_ASSOC)){
			$row['childs']=getCategories($row['id'],$with_count);
			$row['cat_img']=getFullImage($row['cat_img']);
			if(count($row['childs'])>0){
				$row['has_childs']=1;
			}else{
				$row['has_childs']=0;
			}
			if($with_count){
				$row['cnt']=countListing($row['id']);
			}
			$rows[]=$row;
		}
	}
	return $rows;
}


function sendMail($mailData, &$error) {
	global $siteURL, $SMTP_HOST,$SMTP_USER,$SMTP_PASSWORD, $SMTP_FROM,$SMTP_PORT, $SMTP_FROM_EMAIL;
	

	if(trim($mailData ['email'])==""){
		return false;
	}

	$html=file_get_contents($siteURL.'email/common.php');
	$html=str_replace("{BODY}", $mailData['body'], $html);
	if(ENABLE_SENDGRID){
		require_once("sendgrid-php/sendgrid-php.php");
		$sendgrid = new SendGrid(SENDGRID_KEY);
		$email    = new SendGrid\Email();
		$email->addTo($mailData['email']);
		$email->setFrom("noreply@colinaseeds.com");
		$email->setFromName("Colina Seeds");
		$email->setSubject($mailData['sub']);
		$email->setHtml($html);
		//$email->addCategory("access");
		$response = $sendgrid->send($email);
		if($response->getCode()==200) {
			return true;
		}else{
			return false;
		}
	}else{
		require_once 'mailer/PHPMailerAutoload.php';
		
		$mail = new PHPMailer ();
		//$mail->SMTPDebug = 3; // Enable verbose debug output
		$mail->isSMTP (); // Set mailer to use SMTP
		$mail->Host =$SMTP_HOST;// 'mail.leadinfosoft.com'; // Specify main and backup SMTP servers
		$mail->SMTPAuth = true; // Enable SMTP authentication
		$mail->Username =$SMTP_USER;// 'info@leadinfosoft.com'; // SMTP username
		$mail->Password = $SMTP_PASSWORD; // SMTP password
		//$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
		$mail->Port = $SMTP_PORT; // TCP port to connect to

		$mail->From = $SMTP_FROM_EMAIL; //'noreply@leadinfosoft.com';
		$mail->FromName = $SMTP_FROM; // "LoadBoard";
		$mail->addAddress ( $mailData ['email'], $mailData ['name'] ); // Add a recipient
		$mail->isHTML ( true ); // Set email format to HTML

		$mail->Subject = $mailData ['sub'];
		$mail->Body = $html;

		if (! $mail->send ()) {
			// echo 'Message could not be sent.';
			$error = $mail->ErrorInfo;
			return false;
		} else {
			return true;
		}
	}
}
?>
