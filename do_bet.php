<?php
// include 'config.php';
include 'db.php';
include 'functions.php';
db_connect();
function from_obj(&$type,$default = "") {
    return isset($type)? $type : $default;
}

$res = file_get_contents('php://input');
$json = json_decode($res);

$bet_type_id = from_obj( $json->type, "unknown");
$bet_type_name = from_obj( $json->bet_type_name, "undeknown");
$source = from_obj( $json->source, "undeknown");
$is_offer = from_obj( $json->is_offer, "undeknown");
$mode = from_obj( $json->mode, "undeknown");
$each_way = from_obj( $json->each_way, "undeknown");
$bets = from_obj( $json->events, Array());
$amount = from_obj( $json->amount, "undeknown");
$odd_type = from_obj( $json->odd_type, "undeknown");
$currency = from_obj( $json->currency, "undeknown");
$is_live = from_obj( $json->is_live, "undeknown");
$payout = from_obj( $json->payout, "undeknown");
$possible_win = from_obj( $json->possible_win, "undeknown");
$accept_type_id = from_obj( $json->accept_type_id, "undeknown");
$system_min_count = from_obj( $json->system_min_count, "undeknown");
$client_login = from_obj( $json->client_login, "undeknown");
$barcode = from_obj( $json->barcode, "undeknown");
$calc_date = from_obj( $json->calc_date, "undeknown");
$date_time = from_obj( $json->date_time, "undeknown");
$client_bonus_id = from_obj( $json->client_bonus_id, "undeknown");
$is_super_bet = from_obj( $json->is_super_bet, "undeknown");
$is_bonus_money = from_obj( $json->is_bonus_money, "undeknown");
$draw_number = from_obj( $json->draw_number, "undeknown");
$tags = from_obj( $json->tags, "undeknown");
$old_bet_id = from_obj( $json->amount, "old_bet_id");
$auto_cash_out_amount = from_obj( $json->auto_cash_out_amount, "undeknown");
$additional_amount = from_obj( $json->additional_amount, "undeknown");
$total_partial_cashout_amount = from_obj( $json->total_partial_cashout_amount, "undeknown");
$remaining_stake = from_obj( $json->remaining_stake, "undeknown");
$tax_amount = from_obj( $json->tax_amount, "undeknown");
$additional_info = from_obj( $json->additional_info, "undeknown");
$has_recalculation_reason = from_obj( $json->has_recalculation_reason, "undeknown");
$info_cashdesk_id = from_obj( $json->info_cashdesk_id, "undeknown");
$stake_tax_amount = from_obj( $json->stake_tax_amount, "undeknown");
$uid = from_obj( $json->uid, "undeknown");
$response = array();


    $bets_paramts=array(
        "type"=>$bet_type_id,
        "uid"=>$uid,
        "source"=>$source,
        "currency"=>$currency,
        "is_live"=>$is_live,
        "payout"=>$payout,
        "possible_win"=>$possible_win,
        "accept_type_id"=>$accept_type_id,
        "system_min_count"=>$system_min_count,
        "client_login"=>$client_login,
        "barcode"=>$barcode,
        "calc_date"=>$calc_date,
        "date_time"=>$date_time,
        "client_bonus_id"=>$client_bonus_id,
        "is_super_bet"=>$is_super_bet,
        "is_bonus_money"=>$is_bonus_money,
        "draw_number"=>$draw_number,
        "tags"=>$tags,
        "old_bet_id"=>$old_bet_id,
        "auto_cash_out_amount"=>$auto_cash_out_amount,
        "additional_amount"=>$additional_amount,
        "total_partial_cashout_amount"=>$total_partial_cashout_amount,
        "remaining_stake"=>$remaining_stake,
        "tax_amount"=>$tax_amount,
        "additional_info"=>$additional_info,
        "has_recalculation_reason"=>$has_recalculation_reason,
        "info_cashdesk_id"=>$info_cashdesk_id,
        "stake_tax_amount"=>$stake_tax_amount,
        "each_way"=>$each_way,
        "amount"=>$amount,      
        "odd_type"=>$odd_type,
    );
    $bets_slip_id=insertRow("bets_tmp",$bets_paramts);
    
    foreach ($bets as $bet) 
    {
        // $event_id = $bet->event_id;
        $selection_id = from_obj( $bet->selection_id, 0);
        $coeficient = from_obj( $bet->coeficient, "undeknown");
        $outcome = from_obj( $bet->outcome, 0);
        $outcome_name =from_obj(  $bet->outcome_name, "");
        $game_info = from_obj( $bet->game_info, "undeknown");
        $event_name = from_obj( $bet->event_name, "undeknown");
        $game_start_date =from_obj(  $bet->game_start_date, "undeknown");
        $team1 = from_obj( $bet->team1, "undeknown");
        $team2 = from_obj( $bet->team2, "undeknown");
        $competition_name = from_obj( $bet->competition_name, "undeknown");
        $game_id = from_obj( $bet->game_id, "undeknown");
        $sport_id = from_obj( $bet->sport_id, "undeknown");
        $sport_name = from_obj( $bet->sport_name, "undeknown");
        $sport_index =from_obj(  $bet->sport_index, "undeknown");
        $region_name = from_obj( $bet->region_name, "undeknown");
        $market_name =from_obj(  $bet->market_name, "undeknown");
        $match_display_id = from_obj( $bet->match_display_id, "undeknown");
        $game_name = from_obj( $bet->game_name, "undeknown");
        $basis = from_obj( $bet->basis, "undeknown");
        $match_info = from_obj( $bet->match_info, "undeknown");
        $info =from_obj(  $bet->info, "undeknown");
        $home_score = from_obj( $bet->home_score, "undeknown");
        $away_score = from_obj( $bet->away_score, "undeknown");
        $cash_out_price =from_obj(  $bet->cash_out_price, "undeknown");
        $selection_price = from_obj( $bet->selection_price, "undeknown");
        $bet_arra_paramts=array(
            "bet_id"=>$bets_slip_id,
            "selection_id"=>$selection_id,
            "coeficient"=>$coeficient,
            "outcome"=>$outcome,
            "outcome_name"=>$outcome_name,
            "game_info"=>$game_info,
            "event_name"=>$event_name,
            "game_start_date"=>$game_start_date,
            "team1"=>$team1,
            "team2"=>$team2,
            "competition_name"=>$competition_name,
            "game_id"=>$game_id,
            "sport_id"=>$sport_id,
            "sport_name"=>$sport_name,
            "sport_index"=>$sport_index,
            "region_name"=>$region_name,
            "market_name"=>$market_name,
            "match_display_id"=>$match_display_id,
            "game_name"=>$game_name,
            "basis"=>$basis,
            "match_info"=>$match_info,
            "info"=>$info,
            "home_score"=>$home_score,
            "away_score"=>$away_score,
            "cash_out_price"=>$cash_out_price,
            "selection_price"=>$selection_price,
        );
        $sbets_slip_id=insertRow("bet_slip_tmp",$bet_arra_paramts);
      }
if($bets_slip_id){
    
$queryU = $con->prepare ( "SELECT balance
FROM `users`
 WHERE id=:uid
   " );
$queryU->bindParam(":uid", $uid);
$queryU->execute ();
$userData = array ();
if ($queryU->rowCount () > 0) {
$userData= $queryU->fetch ( PDO::FETCH_ASSOC );


    $totalAmout = (float)$userData['balance'] - (float)$amount;
    $paramts=array(
        "balance"=>$totalAmout,
        );
        $id=updateRow("users",$paramts,array("id"=>$uid));
    
    }
$query = $con->prepare ( "SELECT bt.id,bt.amount,bt.type ,bt.k,bt.bonus_bet_amount,bt.is_live
         FROM `bets_tmp` bt
          WHERE bt.id=:id
            " );
    $query->bindParam(":id", $bets_slip_id);
  $query->execute ();
    $batsData = array ();
    if ($query->rowCount () > 0) {
    $batsData= $query->fetch ( PDO::FETCH_ASSOC );

    $queryE = $con->prepare ( "SELECT selection_id,game_id,competition_name  competition, home_score home_team,away_score away_team, game_start_date start_date,market_name market_name,event_name event_name,info m_arjel_code 
         FROM `bet_slip_tmp`
          WHERE bet_id=:id
            " );
    $queryE->bindParam(":id", $bets_slip_id);
  $queryE->execute ();
    $eventData = array ();
    if ($queryE->rowCount () > 0) {
        $eventData= $queryE->fetchAll ( PDO::FETCH_ASSOC );
    $batsData['events'] = $eventData;
    $batsData['is_superbet'] = false;
    $batsData['FreeBetAmount'] = 0.0;
    if($batsData['is_live'] === '1'){
        $batsData['IsLive'] = true;
    }else{
        $batsData['IsLive'] = false;
    }
   
    
    $response['success'] = "OK";
    $response['result'] = "OK";
    $response['result_text']=null;
    $response['details'] = $batsData;
    echo json_encode($response);  
    }
    
}
} else {
        echo "false";
}

