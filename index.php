<?php 
//  include "login.php";
?>
<!doctype html>
<html dir="{{::(conf.availableLanguages[env.lang].rtl ? 'rtl' : 'ltr')}}"
      ng-class="{'integration-auto-height': confPartner.notifyOnResize}"
      lang="{{::(conf.availableLanguages[env.lang].short.toLowerCase())}}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="utf-8">
    <script src="https://my.hellobar.com/09df6853a04aabdc0c0ed5e7b9b116fde686359b.js" type="text/javascript" charset="utf-8" async="async"></script>

    <title ng-bind="siteTitle">Vbet - Sport betting, Poker, Casino, Online Games</title>
    <meta name="description" content="Vbet Bookmaking Company is one of the leading betting sites. Wide range of sports live streams of football, basketball, volleyball, tennis, ice hockey, horse racing etc. About 28,000 live matches yearly, poker events, casino, backgammon and 24 hours of Customer Support.">
    <meta name="keywords" content="sport betting,live sports betting,online betting,bet and win,online football,bet online,soccer bets,champions league,barclays premier league,football betting site">
    <link rel="icon" href="favicon.ico?=20191121072541" />
    <link rel="stylesheet" href="skins/vbet.com/css/skin.min.css?20191121072541"/>
    <!--[if lt IE 10]>
    <script src="//cdn.rawgit.com/jpillora/xdomain/0.7.3/dist/xdomain.min.js"></script>
    <script>xdomain.slaves();</script>
    <![endif]-->

    <link ng-if="conf.customLanguageCss.indexOf(env.lang) !== -1" rel="stylesheet" ng-href="css/fonts/{{env.lang}}.css"/>

    <link ng-if="conf.customLanguageCss.indexOf(env.lang) !== -1" rel="stylesheet" ng-href="skins/{{conf.skin}}/css/fonts/{{env.lang}}.css"/>
    <!-- /build -->

    <link rel="stylesheet" href="custom.css"/></head>
    <body class="{{::conf.site_name.split('.').join('')}}">
    <div class="
        body-wrapper
        {{conf.integrationMode ? 'integration': ''}}
        {{casinoGameOpened ? 'full-screen' : ''}}
        {{env.sliderContent ? 'slider-open' : ''}}
        {{noClassicScroll ? 'no-classic-scroll' : ''}}
        {{$location.path() == '/' ? 'homepage' : ''}}
        {{$location.path().indexOf('customsport') !== -1 ? 'sport' : ''}}
        {{footerMovable ? 'footer-movable' : ''}}
        lang-{{::env.lang}}
        {{$location.path().split('/').join('')}}
        theme-{{theme.id}}
        {{::conf.site_name}}
        {{::domainClass}}
{{(['/poolbetting/', '/freebet/'].indexOf($location.path()) > -1) ? 'modern' : ['/dashboard/', '/livecalendar/', '/results/', '/multiview/', '/overview/', '/virtualsports/', '/insvirtualsports/'].indexOf($location.path()) > -1? 'classic' : conf.sportsLayout}}
        {{env.bodyWrapperClass||''}}
    " no-animate>

        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular.min.js?v=1.7.7"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular-route.min.js?v=1.7.7"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular-animate.min.js?v=1.7.7"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular-cookies.min.js?v=1.7.7"></script>

        <script id="app-config" data-config-url-path="" data-id="" data-config-is-externall=""></script>
        <script src="app.min.js" id="bc-main-js"></script>
        <div class="contain-header tpl-container block-size-visible-b" ng-include="::'templates/header.html'|fixPath" ng-class="{'subheader-enabled': $root.currentPage.hasSubHeader, 'vertical-nav-null': $root.leftMenuOpen === false }"></div>
        <div ng-view ng-class="$location.path().split('/').join('')" class="view-container"></div>
        <div class="footer-group">
            <div class="footer-cell">
                <div class="contain-footer-block"
                     id="footerContainer"
                     ng-hide="!footerMovable && casinoGameOpened > 0"
                     ng-class="{'active': footerOpen && !env.sliderContent, 'footer-fixed': footerMovable}"
                     ng-include="::'templates/footer.html'|fixPath"
                     ng-if="$location.path() !='/popup/'"
                ></div>
                <div class="footer-button theme-{{theme.id}}" ng-click="footerOpen=!footerOpen" ng-class="{'footer-button-open': footerOpen}" ng-hide="conf.footer.disable || !footerMovable || env.sliderContent"></div>
            </div>
        </div>
</div>


</body>
</html>
