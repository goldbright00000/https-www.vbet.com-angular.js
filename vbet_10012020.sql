-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2020 at 06:27 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vbet`
--

-- --------------------------------------------------------

--
-- Table structure for table `bets`
--

CREATE TABLE IF NOT EXISTS `bets` (
  `id` int(11) NOT NULL,
  `event_id` varchar(10) NOT NULL,
  `price` double NOT NULL,
  `bet_slip_id` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bets`
--

INSERT INTO `bets` (`id`, `event_id`, `price`, `bet_slip_id`) VALUES
(4, '1224611047', 3.31, 10),
(5, '1224611047', 3.41, 11),
(6, '1224611047', 2.5, 12),
(7, '1224611047', 2.28, 13);

-- --------------------------------------------------------

--
-- Table structure for table `bet_slip`
--

CREATE TABLE IF NOT EXISTS `bet_slip` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `bet_type_id` int(3) NOT NULL,
  `odd_type` int(1) NOT NULL,
  `source` varchar(5) NOT NULL,
  `is_offer` int(4) NOT NULL,
  `mode` int(3) NOT NULL,
  `each_way` tinyint(1) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `stake` varchar(30) NOT NULL,
  `odds` varchar(30) NOT NULL,
  `outcome` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `bet_type_name` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bet_slip`
--

INSERT INTO `bet_slip` (`id`, `date`, `bet_type_id`, `odd_type`, `source`, `is_offer`, `mode`, `each_way`, `amount`, `stake`, `odds`, `outcome`, `status`, `bet_type_name`) VALUES
(8, '2020-01-11 01:02:56', 1, 0, '50', 0, 1, 0, 60, '', '', '', '', ''),
(9, '2020-01-11 01:03:39', 1, 0, '500', 0, 1, 0, 60, '', '', '', '', ''),
(10, '2020-01-11 01:24:38', 1, 0, '42', 0, 1, 0, 1, '', '', '', '', ''),
(11, '2020-01-11 01:29:02', 1, 0, '42', 0, 1, 0, 1, '', '', '', '', ''),
(12, '2020-01-11 02:38:52', 1, 0, '42', 0, 1, 0, 1, '', '', '', '', '1'),
(13, '2020-01-11 02:40:06', 1, 0, '42', 0, 1, 0, 1, '', '', '', '', 'Single');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL,
  `currency_id` varchar(150) NOT NULL,
  `currency_name` varchar(150) NOT NULL,
  `currency` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_name`, `currency`) VALUES
(1, 'INR', 'Indian rupee ', 'INR'),
(2, 'USD', 'United States Dollar', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `unique_id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name` varchar(300) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `balance` double DEFAULT '0',
  `reg_info_incomplete` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `reg_date` date NOT NULL,
  `birth_date` date NOT NULL,
  `doc_number` varchar(100) NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `casino_balance` double NOT NULL DEFAULT '0',
  `exclude_date` date NOT NULL,
  `bonus_id` int(11) NOT NULL,
  `games` varchar(100) NOT NULL,
  `super_bet` varchar(200) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `doc_issued_by` varchar(200) NOT NULL,
  `doc_issue_date` date NOT NULL,
  `doc_issue_code` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `iban` varchar(100) NOT NULL,
  `active_step` varchar(100) NOT NULL,
  `active_step_state` varchar(100) NOT NULL,
  `subscribed_to_news` tinyint(1) NOT NULL,
  `bonus_balance` double NOT NULL,
  `frozen_balance` double NOT NULL,
  `bonus_win_balance` double NOT NULL,
  `city` varchar(200) NOT NULL,
  `has_free_bets` tinyint(1) NOT NULL,
  `loyalty_point` int(100) NOT NULL,
  `loyalty_earned_points` int(100) NOT NULL,
  `loyalty_exchanged_points` int(100) NOT NULL,
  `loyalty_level_id` int(100) NOT NULL,
  `affiliate_id` int(100) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `incorrect_fields` int(100) NOT NULL,
  `loyalty_point_usage_period` int(100) NOT NULL,
  `loyalty_min_exchange_point` int(100) NOT NULL,
  `loyalty_max_exchange_point` int(100) NOT NULL,
  `active_time_in_casino` int(100) NOT NULL,
  `last_read_message` varchar(200) NOT NULL,
  `unread_count` int(100) NOT NULL,
  `last_login_date` date NOT NULL,
  `swift_code` varchar(100) NOT NULL,
  `counter_offer_min_amount` int(100) NOT NULL,
  `sportsbook_profile_id` int(100) NOT NULL,
  `subscribe_to_email` tinyint(1) NOT NULL,
  `subscribe_to_sms` tinyint(1) NOT NULL,
  `subscribe_to_bonus` tinyint(1) NOT NULL,
  `is_tax_applicable` tinyint(1) NOT NULL,
  `nem_id_token` varchar(100) NOT NULL,
  `mobile_phone` varchar(100) NOT NULL,
  `personal_id` int(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `additional_address` varchar(300) NOT NULL,
  `btag` varchar(100) NOT NULL,
  `birth_region` varchar(100) NOT NULL,
  `supported_currencies` varchar(100) NOT NULL,
  `wallets` varchar(100) NOT NULL,
  `previous_login_time` date NOT NULL,
  `nick_name` varchar(100) NOT NULL,
  `is_super_bet_available` varchar(100) NOT NULL,
  `is_gdpr_passed` varchar(100) NOT NULL,
  `subscribe_to_internal_message` varchar(100) NOT NULL,
  `subscribe_to_push_notification` tinyint(1) NOT NULL,
  `subscribe_to_phone_call` tinyint(1) NOT NULL,
  `title` varchar(100) NOT NULL,
  `terms_and_conditions_version` varchar(100) NOT NULL,
  `terms_and_conditions_acceptance_date` varchar(100) NOT NULL,
  `last_preferred_currency` varchar(100) NOT NULL,
  `is_agent` tinyint(1) NOT NULL,
  `is_two_factor_authentication_enabled` int(100) NOT NULL,
  `authentication_status` int(100) NOT NULL,
  `qr_code_origin` varchar(100) NOT NULL,
  `session_duration` varchar(100) NOT NULL,
  `is_new_client` varchar(100) NOT NULL,
  `unplayed_balance` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `casino_unplayed_balance` varchar(100) NOT NULL,
  `client_notifications` varchar(100) NOT NULL,
  `bonus_money` varchar(100) NOT NULL,
  `loyalty_last_earned_points` varchar(100) NOT NULL,
  `is_cash_out_available` tinyint(1) NOT NULL,
  `sur_name` varchar(100) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `calculatedBalance` int(11) NOT NULL,
  `calculatedBonus` int(11) NOT NULL,
  `skype_request` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `username`, `name`, `first_name`, `last_name`, `middle_name`, `gender`, `email`, `phone`, `password`, `balance`, `reg_info_incomplete`, `address`, `reg_date`, `birth_date`, `doc_number`, `currency_id`, `casino_balance`, `exclude_date`, `bonus_id`, `games`, `super_bet`, `country_code`, `doc_issued_by`, `doc_issue_date`, `doc_issue_code`, `province`, `iban`, `active_step`, `active_step_state`, `subscribed_to_news`, `bonus_balance`, `frozen_balance`, `bonus_win_balance`, `city`, `has_free_bets`, `loyalty_point`, `loyalty_earned_points`, `loyalty_exchanged_points`, `loyalty_level_id`, `affiliate_id`, `is_verified`, `incorrect_fields`, `loyalty_point_usage_period`, `loyalty_min_exchange_point`, `loyalty_max_exchange_point`, `active_time_in_casino`, `last_read_message`, `unread_count`, `last_login_date`, `swift_code`, `counter_offer_min_amount`, `sportsbook_profile_id`, `subscribe_to_email`, `subscribe_to_sms`, `subscribe_to_bonus`, `is_tax_applicable`, `nem_id_token`, `mobile_phone`, `personal_id`, `zip_code`, `additional_address`, `btag`, `birth_region`, `supported_currencies`, `wallets`, `previous_login_time`, `nick_name`, `is_super_bet_available`, `is_gdpr_passed`, `subscribe_to_internal_message`, `subscribe_to_push_notification`, `subscribe_to_phone_call`, `title`, `terms_and_conditions_version`, `terms_and_conditions_acceptance_date`, `last_preferred_currency`, `is_agent`, `is_two_factor_authentication_enabled`, `authentication_status`, `qr_code_origin`, `session_duration`, `is_new_client`, `unplayed_balance`, `language`, `casino_unplayed_balance`, `client_notifications`, `bonus_money`, `loyalty_last_earned_points`, `is_cash_out_available`, `sur_name`, `full_name`, `calculatedBalance`, `calculatedBonus`, `skype_request`) VALUES
(1, 0, '8128500518', '', 'Jayanit', 'Satani', '', '', 'jvsatani@gmail.com', '918128500518', '1234', 0, '', '', '0000-00-00', '0000-00-00', '', 1, 0, '0000-00-00', 0, '', '', '', '', '0000-00-00', '', '', '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', 0, 0, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', '', 0, 0, 0),
(2, 0, 'mahesh', '', 'MAHESH', 'JALODARA', '', '', 'mahesh@gmail.com', '911234567890', '123456', 1000000, '', '', '0000-00-00', '0000-00-00', '', 1, 25, '0000-00-00', 0, '', '', '', '', '0000-00-00', '', '', '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', 0, 0, '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', '', 30, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bets`
--
ALTER TABLE `bets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bet_slip`
--
ALTER TABLE `bet_slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bets`
--
ALTER TABLE `bets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bet_slip`
--
ALTER TABLE `bet_slip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
